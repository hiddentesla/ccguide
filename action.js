'use strict'

function pageReady() {
    setContents();
    $(".seemless_anchor").click(seemlessAnchorClick);
    initVersusTable();
    setArmorHint();
    setWarheadHint();
}

function seemlessAnchorClick() {
    var targetId = this.getAttribute("jumpto");
    goToByScroll(targetId);
    return false;
}

function goToByScroll(id){
    $('html,body').animate({scrollTop: $("#"+id).offset().top},'fast');
    return false;
}

function initVersusTable() {
    versusTable = new Map();

    for (var i in armors) {
        for (var j in warheads) {
            var armor = armors[i];
            var warhead = warheads[j];
            var key = warhead + "_" + armor;
            var cell = $("#" + key).get(0);
            if (cell != undefined) {
                var value_ = cell.innerHTML;
                versusTable.set(key, value_);
            }
        }
    }
}

function setArmorHint() {
    var hints = new Map();
    var hintString = "";
    for (var i in armors) {
        var armor = armors[i];
        var versusArmor = new Map();
        var versusString = "";
        for (var j in warheads) {
            var warhead = warheads[j];
            var key = warhead + "_" + armor;
            var versus = versusTable.get(key);
            if (versus) {
                versusArmor.set(warhead, versus);
                versusString += (warhead + ": " + versus + "%\n" );
            }
        }
        hints.set(armor, versusString);
    }
        
    var elements = $(".armor");
    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        var armor = element.innerHTML;
        var hintString = hints.get(armor);
        if (hintString) {
            element.title = hintString;
        }
    }
}

function setWarheadHint() {
    var hints = new Map();
    var hintString = "";
    
    for (i in warheads) {
        var warhead = warheads[i];
        var versusString = "";
        for (var j in armors) {
            var armor = armors[j];
            var key = warhead + "_" + armor;
            var versus = versusTable.get(key);
            if (versus) {
                versusString += (armor + ": " + versus + "%\n");
            }
        }
        hints.set(warhead, versusString);
    }
    
    var elements = $(".warhead");
    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        var warhead = element.innerHTML;
        if (warhead == "Super") {
                element.title = "To all armors: 100%\n";
        }
        else if (warhead !== "None") {
            warhead = warheadLongToShort(warhead);
            var hintString = hints.get(warhead);
            if (hintString) {
                element.title = hintString;
            }
        }
    }    
}


function warheadLongToShort(longName) {
    switch (longName) {
    case "ArmorPiercing" :
        return "AP";
    case "HiExplosive":
        return "HE";
    case "SmallArms":
        return "SA";
    default:
        return longName;
    }
}

function addListItem(ul, listItemId, jumpto, value) {
    var li = document.createElement("li");
    // id seems not to be of use. Leave it here for future extensibility
    if (listItemId)
        li.setAttribute("id", listItemId);
    li.setAttribute("class", "seemless_anchor");
    li.setAttribute("jumpto", jumpto);
    li.innerHTML = value;
    ul.appendChild(li);
}

function setContents() {
    var table = $("#content_table")[0];
    var elements = $(".content_table_element");
    if (!elements || elements.length <= 0) {
        alert("Cannot find content_table_element!");
        return;
    }

    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        var parentNode = element.parentNode;
        addListItem(table, null, parentNode.id, element.innerHTML);
    }

    return;
    addListItem(table, null, "disambiguous", "名词释义");
    addListItem(table, null, "disambiguous", "名词释义");
    addListItem(table, null, "vehicle", "车辆");
}

var versusTable = null;
var armors = ["Infantry", "Wood", "Light", "Heavy"];
var warheads = ["SA", "HE", "Fire", "AP"];

$(document).ready(pageReady);
